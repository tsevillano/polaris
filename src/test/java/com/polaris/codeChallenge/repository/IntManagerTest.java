package com.polaris.codeChallenge.repository;

import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Random;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
class IntManagerTest {
    @MockBean
    private Random random;
    private IntManager intManager;

    @BeforeEach
    public void setUp() {
        this.intManager = new IntManager(this.random);
    }

    @Test
    void create() {
        when(random.nextInt(Integer.MAX_VALUE)).thenReturn(1);
        Assertions.assertEquals(1,intManager.create());
    }


    @Test
    void createRandomwhile() {
        when(random.nextInt(Integer.MAX_VALUE))
                .thenReturn(1)
                .thenReturn(1)
                .thenReturn(2)
                .thenReturn(1)
                .thenReturn(2)
                .thenReturn(3);
        Assertions.assertEquals(1,intManager.create());
        Assertions.assertEquals(2,intManager.create());
        intManager.getObject();
        Assertions.assertEquals(3,intManager.create());
    }

    @Test
    void  createEspecificInt(){
        intManager.create(1);
        Assertions.assertTrue(intManager.create(1).isEmpty());
    }

    @Test
    void  createEspecificIntLock(){
        intManager.create(1);
        intManager.getLocked();
        Assertions.assertTrue(intManager.create(1).isEmpty());
    }

    @Test
    void getObjectEmptyList(){
        Assertions.assertTrue(intManager.getObject().isEmpty());
    }

    @Test
    void getObject(){
        when(random.nextInt(Integer.MAX_VALUE)).thenReturn(1);
        intManager.create();
        Assertions.assertEquals(1,intManager.getObject().get());
    }


    @Test
    void getObjectEmptyListAndEspecificObject(){
        Assertions.assertTrue(intManager.getObject(1).isEmpty());
    }

    @Test
    void getObjectAndEspecificObject(){
        when(random.nextInt(Integer.MAX_VALUE)).thenReturn(1);
        intManager.create();
        Assertions.assertEquals(1,intManager.getObject(1).get());
    }

    @Test
    void getObjectAndEspecificObjectAndInvalidObject(){
        when(random.nextInt()).thenReturn(1);
        intManager.create();
        Assertions.assertTrue(intManager.getObject(2).isEmpty());
    }

    @Test
    void returnObjectAndEspecificObject(){
        when(random.nextInt(Integer.MAX_VALUE)).thenReturn(1);
        intManager.create();
        intManager.getObject(1);
        Assertions.assertTrue(intManager.returnObject(1));
    }

    @Test
    void returnObjectAndEspecificObjectAndInvalidObject(){
        when(random.nextInt()).thenReturn(1);
        intManager.create();
        Assertions.assertFalse(intManager.returnObject(2));
    }

    @Test
    void returnObjectWithEmptyList(){
        Assertions.assertFalse(intManager.returnObject(1));
    }

    @Test
    void retrunLockedList(){
        Assertions.assertTrue(intManager.getLocked().isEmpty());
    }
    @Test
    void retrunUnLockedList(){
        Assertions.assertTrue(intManager.getUnlocked().isEmpty());
    }
}