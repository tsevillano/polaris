package com.polaris.codeChallenge.service;

import com.polaris.codeChallenge.dto.ResultsDTO;
import com.polaris.codeChallenge.exception.ObjectErrorException;
import com.polaris.codeChallenge.repository.IntManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Collections;
import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
@SpringBootTest
class ObjectServiceTest {
    @MockBean
    private IntManager intManager;
    @Autowired
    private ObjectService objectService;


    @Test
    void create() {
        Integer expectedValue = 1;
        when(intManager.create()).thenReturn(expectedValue);
        Integer val = this.objectService.create();
        Assertions.assertEquals(expectedValue, val);
        verify(intManager).create();
    }

    @Test
    void getObjectFromPool() throws ObjectErrorException {
        when(intManager.getObject()).thenReturn(Optional.of(1));
        Integer result = objectService.getObjectFromPool();
        Assertions.assertEquals(1,result);

    }

    @Test
    void testGetObjectFromPool() throws ObjectErrorException {
        Integer expectedValue = 1;
        when(intManager.getObject(expectedValue)).thenReturn(Optional.of(1));
        Integer result = objectService.getObjectFromPool(expectedValue.toString());
        Assertions.assertEquals(expectedValue,result);
    }

    @Test
    void testGetObjectFromPoolInvalidNumberException() throws ObjectErrorException {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Integer result = objectService.getObjectFromPool("not a number");
        });
    }
    @Test
    void testGetObjectFromPoolObjectNotFoundException() throws ObjectErrorException {
        Assertions.assertThrows(ObjectErrorException.class, () -> {
            when(intManager.getObject(1)).thenReturn(Optional.empty());
            Integer result = objectService.getObjectFromPool("1");
        });
    }


    @Test
    void returnObjectToThePool() throws ObjectErrorException {
        Integer expectedValue = 1;
        when(intManager.returnObject(expectedValue)).thenReturn(true);
        objectService.returnObjectToThePool(expectedValue.toString());
        verify(intManager).returnObject(expectedValue);
    }

    @Test
    void returnObjectToThePooInvalidNumberException() throws ObjectErrorException {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            objectService.returnObjectToThePool("not a number");
        });
    }

    @Test
    void returnObjectToThePooInvalidNumberException2() throws ObjectErrorException {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            objectService.getObjectFromPool("-100");
        });
    }

    @Test
    void returnObjectToThePooNumberNotFound() throws ObjectErrorException {
        Assertions.assertThrows(ObjectErrorException.class, () -> {
            when(intManager.returnObject(1)).thenReturn(false);
            objectService.returnObjectToThePool("1");
        });
    }


    @Test
    void retriveList() throws ObjectErrorException {
        when(intManager.getLocked()).thenReturn(Collections.emptyList());
        when(intManager.getUnlocked()).thenReturn(Collections.emptyList());
        ResultsDTO<Integer> result = objectService.retriveList();
        Assertions.assertTrue(result.getUnlocked().isEmpty());
        Assertions.assertTrue(result.getLocked().isEmpty());
    }
}