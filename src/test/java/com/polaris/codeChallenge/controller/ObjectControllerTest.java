package com.polaris.codeChallenge.controller;

import com.polaris.codeChallenge.dto.ResultsDTO;
import com.polaris.codeChallenge.exception.ObjectErrorException;
import com.polaris.codeChallenge.service.ObjectService;
import org.assertj.core.util.Strings;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ObjectController.class)
class ObjectControllerTest {
    private static final String API_PREFIX = "/v1/objects";
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ObjectService objectService;


    @Test
    void create() throws Exception {
        when(objectService.create()).thenReturn(1);
        this.mockMvc.perform(post(API_PREFIX)
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }

    @Test
    void getAnyObject() throws ObjectErrorException, Exception {
        when(objectService.getObjectFromPool()).thenReturn(1);
        this.mockMvc.perform(put(API_PREFIX)
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void getSpecificObject() throws ObjectErrorException, Exception {
        when(objectService.getObjectFromPool("1")).thenReturn(1);
        this.mockMvc.perform(put(Strings.concat(API_PREFIX, "/1"))
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void returnObject() throws ObjectErrorException, Exception {
        doNothing().when(objectService).returnObjectToThePool("1");
        this.mockMvc.perform(put(Strings.concat(API_PREFIX, "/1/return"))
                .contentType("application/json"))
                .andExpect(status().isAccepted());
    }

    @Test
    void getList() throws Exception {
        when(objectService.retriveList()).thenReturn(new ResultsDTO<Integer>(Collections.EMPTY_LIST, Collections.EMPTY_LIST));
        this.mockMvc.perform(get(Strings.concat(API_PREFIX))
                .contentType("application/json"))
                .andExpect(status().isOk());
    }
}