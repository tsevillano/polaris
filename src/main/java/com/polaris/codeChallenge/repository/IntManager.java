package com.polaris.codeChallenge.repository;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Random;

@Repository
public class IntManager extends Manager<Integer> {

    final private Random random;

    public IntManager(@Value("#{new java.util.Random()}") Random random) {
        super();
        this.random = random;

    }

    /**
     * Add a random number to the list of unlucked objects
     * @return Integer, random number in the pool of unlucked.
     */
    public Integer create() {
        Integer obj = getARandomNumber();
        return super.create(obj).get();
    }

    public Optional<Integer> create(Integer intValue) {
        if (this.getLocked().contains(intValue) || this.getUnlocked().contains(intValue)) {
            return Optional.empty();
        }
        return super.create(intValue);
    }

    private Integer getARandomNumber() {
        Integer obj = null;
        do {
            obj = this.random.nextInt(Integer.MAX_VALUE);
        }while(this.getUnlocked().contains(obj)  || this.getLocked().contains(obj));
        return obj;
    }
}
