package com.polaris.codeChallenge.repository;

import org.springframework.context.annotation.Scope;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Scope("singleton")
public abstract class Manager<T> {
    protected final List<T> locked;
    protected final List<T> unlocked;

    public Manager() {
        this.locked = new ArrayList<>();
        this.unlocked = new ArrayList<>();
    }

    /**
     * Add the Object to the unlucked list.
     * @param obj
     * @return the object.
     */

    public Optional<T> create(T obj) {
        this.unlocked.add(obj);
        return Optional.of(obj);
    }

    /**
     * get the first object from the unlucked and put in the locked list.
     * @return
     */
    public synchronized Optional<T> getObject() {
        if (!unlocked.isEmpty()) {
            T obj = unlocked.remove(0);
            locked.add(obj);
            return Optional.of(obj);
        }
        return Optional.empty();
    }

    /**
     * Get the specific object from the list and put in the lock list.
     * @param obj
     * @return
     */

    public synchronized Optional<T> getObject(T obj) {
        if (unlocked.contains(obj) && unlocked.remove(obj)) {
            locked.add(obj);
            return Optional.of(obj);
        } else {
            return Optional.empty();
        }
    }

    /**
     * return the object from the lock list to the unluck list.
     * @param obj
     * @return
     */
    public synchronized boolean returnObject(T obj) {
        if (locked.contains(obj) && locked.remove(obj)) {
            unlocked.add(obj);
            return true;
        } else {
            return false;
        }
    }

    /**
     * get the list of locked objects.
     * @return
     */
    public List<T> getLocked() {
        return locked;
    }

    /**
     * get the unlucked list.
     * @return
     */

    public List<T> getUnlocked() {
        return unlocked;
    }
}
