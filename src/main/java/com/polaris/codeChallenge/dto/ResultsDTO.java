package com.polaris.codeChallenge.dto;

import java.util.List;

public class ResultsDTO<T> {
    private final List<T> locked;
    private final List<T> unlocked;

    public List<T> getLocked() {
        return locked;
    }

    public List<T> getUnlocked() {
        return unlocked;
    }

    public ResultsDTO(List<T> locked, List<T> unlocked) {
        this.locked = locked;
        this.unlocked = unlocked;
    }
}
