package com.polaris.codeChallenge.exception;

public class ObjectErrorException extends Exception{
    public ObjectErrorException(String msg) {
        super(msg);
    }

    public ObjectErrorException() {
        super("the object is taken by other or not exist");
    }
}
