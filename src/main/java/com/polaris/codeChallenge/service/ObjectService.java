package com.polaris.codeChallenge.service;

import com.polaris.codeChallenge.dto.ResultsDTO;
import com.polaris.codeChallenge.exception.ObjectErrorException;
import com.polaris.codeChallenge.repository.IntManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ObjectService {
    final private IntManager objectManager;

    @Autowired
    public ObjectService(IntManager objectManager) {
        this.objectManager = objectManager;
    }

    /**
     * buisnes logic to create a new object.
     * @return
     */
    public Integer create() {
        return this.objectManager.create();
    }

    /**
     * buisnes logic a specific new object.
     * @return
     */
    public Integer create(String value) throws ObjectErrorException {
        try {
            int obj = Integer.parseInt(value);
            if (obj < 1){
                throw new IllegalArgumentException("the number need to be greater than 1");
            }
            return this.objectManager.create(obj).orElseThrow(ObjectErrorException::new);
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("the id is  not a number");
        }
    }

    /**
     * Logic to retun the object or throw an exception
     * @return
     * @throws ObjectErrorException
     */
    public Integer getObjectFromPool() throws ObjectErrorException {
        return this.objectManager.getObject().orElseThrow(ObjectErrorException::new);
    }

    /**
     * Logic to retun the object or throw an exception
     * @param integerValue
     * @return
     * @throws ObjectErrorException
     */
    public Integer getObjectFromPool(String integerValue) throws ObjectErrorException {
        try {
            int obj = Integer.parseInt(integerValue);
            if (obj < 1){
                throw new IllegalArgumentException("the number need to be greater than 1");
            }
            return this.objectManager.getObject(obj).orElseThrow(ObjectErrorException::new);
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("the id is  not a number");
        }
    }

    /**
     * Logic to retun the object to the unlucked list or throw an exception
     * @param integerValue
     * @throws ObjectErrorException
     */
    public void returnObjectToThePool(String integerValue) throws ObjectErrorException {
        try {
            Integer obj = Integer.parseInt(integerValue);
            if (!this.objectManager.returnObject(obj)) {
                throw new ObjectErrorException("the id was not found");
            }
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("the id is  not a number");
        }
    }

    public ResultsDTO<Integer> retriveList() {
        return new ResultsDTO<>(objectManager.getLocked(), objectManager.getUnlocked());
    }
}
