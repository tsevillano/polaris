package com.polaris.codeChallenge.controller;


import com.polaris.codeChallenge.dto.ResultsDTO;
import com.polaris.codeChallenge.exception.ObjectErrorException;
import com.polaris.codeChallenge.service.ObjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1/objects", produces = "application/json", consumes = "application/json")
@Api(value = "ObjectController", description = "REST APIs related to  object manager!!!!", produces = "application/json", consumes = "application/json")
public class ObjectController {
    private final ObjectService objectService;

    @Autowired
    public ObjectController(ObjectService objectService) {
        this.objectService = objectService;
    }


    @PostMapping(path = "")
    @ApiOperation(value = "Create a  new integer object", response = Integer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "return the createad object."),
    })

    public ResponseEntity<Integer> create() {
        return ResponseEntity.status(HttpStatus.CREATED).body(objectService.create());
    }

    @PostMapping(path = "{id}")
    @ApiOperation(value = "Create a  new integer object", response = Integer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "return the createad object."),
            @ApiResponse(code = 400, message = "The value exist or is not a number"),
    })
    public ResponseEntity<Integer> createWithSpecificId(@PathVariable("id") String value) throws ObjectErrorException {
        return ResponseEntity.status(HttpStatus.CREATED).body(objectService.create(value));
    }

    @GetMapping(path = "")
    @ApiOperation(value = "return the list of luck and unluck objects", response = ResultsDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "return the complete list of object dived in locked and unlocked."),
    })
    public ResponseEntity<ResultsDTO<Integer>> getList() {
        return ResponseEntity.status(HttpStatus.OK).body(objectService.retriveList());
    }

    @PutMapping(path = "")
    @ApiOperation(value = "return the first unlucked object", response = Integer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "return the first unlucked object."),
    })
    public ResponseEntity<Integer> getAnyObject() throws ObjectErrorException {
        return ResponseEntity.ok(objectService.getObjectFromPool());
    }

    @PutMapping(path = "/{id}")
    @ApiOperation(value = "return the specific unlucked object", response = Integer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "return the especific unlucked object."),
            @ApiResponse(code = 400, message = "The value dont exist or is not a number"),
    })
    public ResponseEntity<Integer> getSpecificObject(@PathVariable("id") String integerValue) throws ObjectErrorException {
        return ResponseEntity.ok(objectService.getObjectFromPool(integerValue));
    }

    @ApiOperation(value = "return the specific lucked object to the pool of unlucked", response = Integer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "return the especific unlucked object."),
            @ApiResponse(code = 400, message = "The value dont exist or is not a number"),
    })
    @PutMapping(path = "/{id}/return")
    public ResponseEntity<Void> returnObject(@PathVariable("id") String integerValue) throws ObjectErrorException {
        objectService.returnObjectToThePool(integerValue);
        return ResponseEntity.accepted().build();
    }
}
