Backend Challenge
=======

Design an ObjectMgr which manages a pool of objects. Objects can be considered to be ints (from 1 to n). It should provide api’s to get_object() and free_object().

get_object(): It returns any object , available in the pool. An object cannot be given away again, unless it has been freed.

free_object(int obj): It returns the obj back to the pool, so that it can be given out again.

Discuss the api’s definitions, data structures and write tests.

Follow-on:

Write a Rest API /object to use the above functions to create, get and free objects.

Deploy it as a service/container that can be used to deploy on a linux server.

Feel free to use standard libraries and pick your choice of language.

### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.